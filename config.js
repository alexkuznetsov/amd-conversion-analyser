'use strict';
module.exports = {
    "patterns": ["**/*.js"],
    "exclude": {
        "patterns": [
            "**/**-min.js",
            "**/**.min.js",
            "**/target/**",
            "**/dist/**"
        ],
        "variables": []
    },
    "variableDisplaySize": 128,
    "env": {
        "browser": true,
        "amd": true,
        "qunit": true
    }
};